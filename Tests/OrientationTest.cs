using NUnit.Framework;
using src.Core.Domain;

namespace Tests
{
    public class OrientationTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void NewOrientationMustBeAValueObject()
        {
            var sut1 = new Orientation(CardinalPoints.North);
            var sut2 = new Orientation(CardinalPoints.North);
            var res1 = sut1.TurnLeft();
            var res2 = sut2.TurnLeft();
            Assert.That(sut1, Is.EqualTo(sut2));
            Assert.That(res1, Is.EqualTo(res2));
        }

          [Test]
        public void OrientationHasNoSideEffet()
        {
            var sut1 = new Orientation(CardinalPoints.North);
            var sut2 = new Orientation(CardinalPoints.North);
            var west = sut1.TurnLeft();
            var east = sut2.TurnRight();
            Assert.That(west, Is.EqualTo(new Orientation(CardinalPoints.West)));
            Assert.That(east, Is.EqualTo(new Orientation(CardinalPoints.East)));
        }


        [Test]
        public void FromNorthTurnLeft()
        {
            var sut = new Orientation(CardinalPoints.North);
            var res = sut.TurnLeft();
            Assert.That(res.Value,
                        Is.EqualTo(CardinalPoints.West));
        }


        [Test]
        public void FromEastTurnLeft()
        {
            var sut = new Orientation(CardinalPoints.East);
            var res = sut.TurnLeft();
            Assert.That(res.Value,
                        Is.EqualTo(CardinalPoints.North));
        }

        [Test]
        public void FromNorthTurnLeftFourTimes()
        {
            var sut = new Orientation(CardinalPoints.North);
            var res = sut.TurnLeft().TurnLeft().TurnLeft().TurnLeft();
            Assert.That(res.Value,
                        Is.EqualTo(CardinalPoints.North));
        }

        [Test]
        public void KeepTwoOrientationsIndependant()
        {
            var sut1 = new Orientation(CardinalPoints.North);
            var sut2 = new Orientation(CardinalPoints.East);
            var res1 = sut1.TurnLeft();
            Assert.That(res1.Value,
                        Is.EqualTo(CardinalPoints.West));
            var res2 = sut2.TurnLeft().TurnLeft();
            Assert.That(res2.Value,
                        Is.EqualTo(CardinalPoints.West));

        }

        [Test]
        public void FromWestTurnRightFourTimes()
        {
            var sut = new Orientation(CardinalPoints.West);
            var res = sut.TurnRight().TurnRight().TurnRight();
            Assert.That(res.Value,
                        Is.EqualTo(CardinalPoints.South));
        }
    }
}


