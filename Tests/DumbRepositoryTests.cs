using NUnit.Framework;

using src.ServiceLayer;
using src.Core.Domain;
using src.Core.Behaviors;
using src.Adapters.Dto;
using src;

namespace Tests
{

    public class DumpRepoTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void RequestRover()
        {
            IRepository repo = new DumbRepository();
            var actualPosition = repo.Query("/Rover/0/position");            

            var expectedPosition = new DtoPositionForRepository(0,0, CardinalPoints.North);

            Assert.That(actualPosition, Is.EqualTo(expectedPosition));
        }

        [Test]
        public void StoreRoverPosition()
        {
            var repo = new DumbRepository();
            DtoPosition newPosition = new DtoPosition(new DtoPoint(1,2), CardinalPoints.East);
            repo.Save(newPosition);

            DtoPositionForRepository expectedPosition = new DtoPositionForRepository(1, 2, CardinalPoints.East);
            Assert.That(repo.InternalRepresentation, Is.EqualTo(expectedPosition));

            /*
            var actualPosition = repo.Query("/Rover/0/position");
            DtoPosition expectedPosition = new DtoPosition(new DtoPoint(1,2), CardinalPoints.East);
            Assert.That(actualPosition, Is.EqualTo(expectedPosition));*/
        }

        [Test]
        public void SecondRequestRover()
        {
            var  repo = new DumbRepository();
                        
            repo.InternalRepresentation = new DtoPositionForRepository(1,1 , CardinalPoints.South);
            
            var actualPosition = repo.Query("/Rover/0/position");
            
            var  expectedPosition = new DtoPositionForRepository(1, 1, CardinalPoints.South);

            Assert.That(actualPosition, Is.EqualTo(expectedPosition));
        }
    }
}