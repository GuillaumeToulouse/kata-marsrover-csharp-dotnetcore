using NUnit.Framework;
using src.Core.Domain;

namespace Tests
{

    public class MapTests
    {
        [SetUp]
        public void Setup()
        {
        }

          [Test]
        public void PutARoverOnTheOriginOfTheMapAndMoveForward()
        {
            var sut = new Map(5, 2 );
            var identifiantRover1 =  sut.Land(  new Point(0,0), CardinalPoints.North );

            sut.TalkTo(identifiantRover1);
            sut.SendCommand( "F");

            var expected_position = new Position(new Point(0,1), CardinalPoints.North);            
            Assert.AreEqual( expected_position, sut.Localize()  );
        }

         [Test]
        public void PutTwoRoversOnTheOriginOfTheMapAndMoveOnlyTheFirstForward()
        {
            var map = new Map(5, 2 );
            var identifiantRover1 =  map.Land(  new Point(0,0), CardinalPoints.North );
            var identifiantRover2 = map.Land(  new Point(2,2), CardinalPoints.North );

            map.TalkTo(identifiantRover1);
            map.SendCommand( "F");


            var expected_position1 = new Position(new Point(0,1), CardinalPoints.North);
            var expected_position2 = new Position(new Point(2,2), CardinalPoints.North);

            Assert.AreEqual(expected_position1,  map.Localize()  );

            map.TalkTo(identifiantRover2);
            Assert.AreEqual( expected_position2, map.Localize() );
        }


          [Test]
        public void PutTwoRoversOnTheOriginOfTheMapAndMoveForward()
        {
            var map = new Map(5, 2 );
            var rover1 = map.Land(  new Point(0,0), CardinalPoints.North );
            map.Obstacle(  x: 0, y : 1 );

            map.TalkTo(rover1);
            map.SendCommand("F");

            var expected_position = new Position(new Point(0,0), CardinalPoints.North);
            Assert.AreEqual( expected_position, map.Localize() );
        }

       [Test]
        public void PutTwoRoversOnTheOriginOfTheMapAndMoveForward_2()
        {
            var map = new Map(5, 2 );
            var rover1 = map.Land(  new Point(0,0), CardinalPoints.North );
            map.Obstacle(  x : 0, y :2 );

            map.TalkTo(rover1);
            map.SendCommand("F");
            map.SendCommand("F");
            map.SendCommand("F");

            var expected_position = new Position(new Point(0,1), CardinalPoints.North);
            Assert.AreEqual(  expected_position, map.Localize() );
        }



        [Test]
        public void RoverPositionAtTheUpper_EdgeOfTheMap()
        {
            var map = new Map(maxX : 5, maxY: 2 );
            var rover1 = map.Land(  new Point(0,2), CardinalPoints.North );

            map.TalkTo(rover1);
            map.SendCommand("F");

            var expected_position = new Position(new Point(0,0), CardinalPoints.North);            
            Assert.AreEqual( expected_position, map.Localize()  );
        }


         [Test]
        public void RoverPositionAtTheUpper_EdgeOfTheMap_WithObstacle()
        {
            var map = new Map(maxX : 5, maxY: 2 );
            var rover1 = map.Land(  new Point(0,1), CardinalPoints.North );
            map.Obstacle(x : 0, y : 0);

            map.TalkTo(rover1);
            map.SendCommand("F");
            map.SendCommand("F");

            var expected_position = new Position(new Point(0,2), CardinalPoints.North);            
            Assert.AreEqual( expected_position, map.Localize()  );
        }



         [Test]
        public void RoverPositionAtTheLowerEdgeOfThemMap()
        {
            var map = new Map(maxX: 5, maxY: 2 );
            var rover1 = map.Land(  new Point(0,0), CardinalPoints.South );
                      

            map.TalkTo(rover1);
            map.SendCommand("F");
            

            var expected_position = new Position(new Point(0,2), CardinalPoints.South);            
            Assert.AreEqual( expected_position, map.Localize()  );
        }

          [Test]
        public void RoverPositionAtTheLeftEdgeOfThemMap()
        {
            var map = new Map(5, 2 );
            var rover1 = map.Land(  new Point(0,0), CardinalPoints.West );                      

            map.TalkTo(rover1);
            map.SendCommand("F");            

            var expected_position = new Position(new Point(5,0), CardinalPoints.West);            
            Assert.AreEqual( expected_position, map.Localize()  );
        }

         [Test]
        public void RoverPositionAtTheRightEdgeOfThemMap()
        {
            var map = new Map(5, 2 );
            var rover1 = map.Land(  new Point(5,0), CardinalPoints.East );                      

            map.TalkTo(rover1);
            map.SendCommand("F");            

            var expected_position = new Position(new Point(0,0), CardinalPoints.East);            
            Assert.AreEqual( expected_position, map.Localize()  );
        }

        [Test]
        public void TwoMapsAreEquals(){
            var sutMap1 = new Map(2, 5);
            //sutMap1.Obstacle(x: 1, y: 2);
            var sutMap2 = new Map(2, 5);
            Assert.That(sutMap1, Is.EqualTo(sutMap2));
        }

          [Test]
        public void TwoMapsAreEqualsWithObstacles(){
            var sutMap1 = new Map(2, 5);
            sutMap1.Obstacle(x: 1, y: 2);
            var sutMap2 = new Map(2, 5);
            sutMap2.Obstacle(x: 1, y: 2);
            Assert.That(sutMap1, Is.EqualTo(sutMap2));
        }

           [Test]
        public void TwoMapsAreNotEqualsWithDifferentObstacles(){
            var sutMap1 = new Map(2, 5);
            sutMap1.Obstacle(x: 1, y: 2);
            var sutMap2 = new Map(2, 5);
            sutMap2.Obstacle(x: 3, y: 3);
            Assert.That(sutMap1.EqualObstactle(sutMap2), Is.False);
            Assert.That(sutMap1, Is.Not.EqualTo(sutMap2));
        }

           [Test]
        public void TwoMapsAreNotEqualsWithDifferentNumberOfObstacles(){
            var sutMap1 = new Map(2, 5);
            sutMap1.Obstacle(x: 1, y: 2);
            var sutMap2 = new Map(2, 5);            
            Assert.That(sutMap1.EqualObstactle(sutMap2), Is.False);                   
            Assert.That(sutMap1, Is.Not.EqualTo(sutMap2));
        }

        [Test]
        public void TwoMapsAreNotEqualsWithSameObstaclesInDifferentOrder(){
            var sutMap1 = new Map(2, 5);
            sutMap1.Obstacle(x: 1, y: 2);
            sutMap1.Obstacle(x: 3, y: 3);
            var sutMap2 = new Map(2, 5);
            sutMap2.Obstacle(x: 3, y: 3);
            sutMap2.Obstacle(x: 1, y: 2);
            Assert.That(sutMap1.EqualObstactle(sutMap2), Is.True);
            Assert.That(sutMap1, Is.EqualTo(sutMap2));
        }

    }
}