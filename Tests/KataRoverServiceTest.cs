using NUnit.Framework;

using src.ServiceLayer;
using src.Core.Domain;
using src.Core.Behaviors;
using src.Adapters.Dto;
using src;

namespace Tests
{

    public class KataRoverServiceTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void MapObservation()
        {
            ICanObserveRover sut = new RoverObservationService();  // initialise                
            var actualMap = sut.GiveCurrentMap();
            var expectedMap = new Map(3, 4);
            Assert.That(actualMap, Is.EqualTo(expectedMap));
            // relire attentivement https://herbertograca.com/2017/11/16/explicit-architecture-01-ddd-hexagonal-onion-clean-cqrs-how-i-put-it-all-together/                
        }

        [Test]
        public void SendValidCommand()
        {
            var sut = new RoverDriverService();
            CommandResult res = sut.SendCommand("F");
            CommandResult expected = new ValidCommand();
            Assert.That(res, Is.EqualTo(expected));
        }

        [Test]
        public void SendBadCommand()
        {
            var sut = new RoverDriverService();  // initialise                
            CommandResult res = sut.SendCommand("A");
            CommandResult expected = new InvalidCommand();
            Assert.That(res, Is.EqualTo(expected));
        }

        [Test]
        public void SendValidCommandAndObserveResult()
        {
            IRepository testRepo = new DumbRepository();
            var sut = new RoverDriverService(testRepo);

            CommandResult res = sut.SendCommand("F");

            ICanObserveRover observer = new RoverObservationService(testRepo);
            var actualPosition = observer.GiveCurrentRoverPosition();
            
            var expectedPosition = new Position(point: new Point(0, 1), orientation: CardinalPoints.North);
            Assert.That(actualPosition, Is.EqualTo(expectedPosition));
        }
        //TODO:  utiliser le mapAdapter dans le RoverDriverService
    }
}