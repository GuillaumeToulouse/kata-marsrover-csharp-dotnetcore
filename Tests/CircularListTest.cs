using NUnit.Framework;
using src.Core.Utilities;

namespace Tests
{

    public class CircularListTests
    {
        [SetUp]
        public void Setup()
        {
        }

          [Test]
        public void TestEqualTo()
        {
            var sut1 = new CircularList<string>("A", "B", "C");
            var sut2 = new CircularList<string>("A", "B", "C");
            Assert.That(sut1, Is.EqualTo(sut2));
        }

        [Test]
        public void MoveInsideCircularList()
        {
            var sut = new CircularList<string>("1", "2");
            var res = sut.Next(0);
            Assert.That( res, Is.EqualTo("1"));
            res = sut.Next(1);
            Assert.That( res, Is.EqualTo("2"));
            res = sut.Next(1);
            Assert.That( res, Is.EqualTo("1"));
        }


        [Test]
        public void MoveInsideCircularListWithThreeElements()
        {
            var sut = new CircularList<string>("1", "2", "3");
            var res = sut.Next(0);
            Assert.That( res, Is.EqualTo("1"));
            res = sut.Next(2);
            Assert.That( res, Is.EqualTo("3"));
            //res = sut.Next(1);  similar to the line below
            res = sut.Next((3 * sut.Length) + 1);
            Assert.That( res, Is.EqualTo("1"));
        }

        [Test]
        public void PositionAndMoveInsideCircularListWithThreeElements()
        {
            var sut = new CircularList<string>("A", "B", "C");
            sut.StartAt("B");
            var res = sut.Next(1);
            Assert.That( res, Is.EqualTo("C"));
        }

        [Test]
        public void UnchangedPositionWhenNotFoundInsideCircularListWithThreeElements()
        {
            var sut = new CircularList<string>("A", "B", "C");
            sut.StartAt("Z");
            var res = sut.Next(0);
            Assert.That( res, Is.EqualTo("A"));
        }

        [Test]
        public void TestUnEqualTo()
        {
            var sut1 = new CircularList<string>("A", "B", "C");
            var sut2 = new CircularList<string>("A", "B");
            Assert.That(sut1, Is.Not.EqualTo(sut2));
        }

        [Test]
        public void TestUnEqualToWithSameSize()
        {
            var sut1 = new CircularList<string>("A");
            var sut2 = new CircularList<string>("B");
            Assert.That(sut1, Is.Not.EqualTo(sut2));
        }

        [Test]
        public void TestUnEqualWithSameElementsDifferentOrder()
        {
            var sut1 = new CircularList<string>("A", "B");
            var sut2 = new CircularList<string>("B", "A");
            Assert.That(sut1, Is.EqualTo(sut2));
        }

          [Test]
        public void GoingNegativeIsSameThanGoingPositiveModuloTheSizeOfTheList()
        {
            var sut = new CircularList<string>("A", "B", "C", "D");
            sut.StartAt("B");
            var res = sut.Previous(1);
            Assert.That( res, Is.EqualTo("A"));
            sut.StartAt("B");
             res = sut.Next(3);
            Assert.That( res, Is.EqualTo("A"));
        }

        [Test]
        public void GoingPreviousFromFirstPosition()
        {
            var sut = new CircularList<string>("A", "B", "C", "D");
            sut.StartAt("A");
            var res = sut.Previous(1);
            Assert.That( res, Is.EqualTo("D"));
        }

        [Test]
        public void GoingPreviousOverLengthFromFirstPosition()
        {
            var sut = new CircularList<string>("A", "B", "C", "D");
            sut.StartAt("A");
            var res = sut.Previous((3 * sut.Length) + 1);
            Assert.That( res, Is.EqualTo("D"));
        }

    }
}