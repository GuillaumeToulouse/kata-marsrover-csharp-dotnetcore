using NUnit.Framework;
using src.Core.Behaviors;
using Moq;
using src.Core.Domain;
using src.Core.Utilities;
using src.Adapters;
using src.Adapters.Dto;

namespace Tests
{

    public class MapAdapterTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ReadAMapFromAJsonFile_usingRealFileSystem(){
            var sut = new MapAdapter(new IOFileReader() );
            var actualMap = sut.read(filePath : "../../../map.json"); // ici on consomme une ressource externe, donc ce n'est pas un PUR test unitaire (selon M.Feather)
            // POUR:  pas de mock compliqués à lire; plus proche de la réalité
            // CONTRE:  il faut ouvrir et lire le contenu du fichier json pour comprendre le test
            
            //var expectedMap = new MapForScenario(abcisseMax : 3, ordonneeMax: 4);
            var expectedMap = new Map( 3,  4);
            Assert.That(expectedMap, Is.EqualTo(actualMap));
        }       

        [Test]
        public void ReadAMapFromAJsonFile_usingMock(){
            //le Mock rend le test complètement indépendant de ressources externes.
            var mockedFileReader = new Mock<ICanReadFile>();
            mockedFileReader.Setup( (m)=> m.ReadAllText(It.IsAny<string>()) ).Returns(@"{    ""LargeurCarte"" : 3,    ""LongueurCarte"" : 4}");     
            //CONTRE: si l'implémentation de  ICanReadFile change; on est obligé de changer l'armement du Mock     (partout!!!!)   
            var sut = new MapAdapter( mockedFileReader.Object );

            var actualMap = sut.read(filePath : "whatever.json");

            ///var expectedMap = new MapForScenario(abcisseMax: 3, ordonneeMax: 4);
            var expectedMap = new Map( 3, 4);
            Assert.That(expectedMap, Is.EqualTo(actualMap));
        } 

        // 3e  FORMULE/ utiliser une librairie  https://github.com/System-IO-Abstractions/System.IO.Abstractions
        // dotnet add package System.IO.Abstractions.TestingHelpers

    }
}