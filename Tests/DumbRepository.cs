using System;
using System.Collections.Generic;
using src.Adapters.Dto;
using src.Core.Domain;

namespace src
{
    public class DumbRepository : IRepository
    {
        DtoPositionForRepository _internalRepresentation;

        internal DtoPositionForRepository InternalRepresentation
        {
            get
            { return _internalRepresentation; }
            set {_internalRepresentation = value;}
        }

        public override bool Equals(object obj)
        {
            return obj is DumbRepository repository &&
                   EqualityComparer<DtoPositionForRepository>.Default.Equals(InternalRepresentation, repository.InternalRepresentation);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(InternalRepresentation);
        }

        public DtoPositionForRepository Query(string v)
        {
            return _internalRepresentation;
        }

        public void Save(DtoPosition newPosition)
        {
             _internalRepresentation = new DtoPositionForRepository( newPosition.Point.X, newPosition.Point.Y, newPosition.Orientation );
        }
    }
}