using NUnit.Framework;
using src.Core.Domain;

namespace Tests
{
    public class PointTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestEqualTo()
        {
            var p1 = new Point(0, 1);
            var p2 = new Point(0, 1);
            var p3 = new Point(0, 0);
            Assert.AreEqual(p1, p2);
            Assert.AreNotEqual(p1, p3);
        }

           [Test]
        public void Translate_Delta_X_Y_1_Shall_Return_A_New_Point()
        {
            var sut = new Point(0, 1);
            var result = sut.Translate(1,1);
            var expected = new Point(1, 2);
            Assert.AreEqual(expected, result );
        }

          [Test]
        public void  A_Point_Translate_In_SPace_And_Wrap_On_Y()
        {
            var sut = new Point(x: 0, y: 1, space: new SphericSpace( maxX: 1, maxY: 2));
            var result = sut.Translate(0,2);
            var expected = new Point(x: 0, y: 0, new SphericSpace( maxX: 1, maxY: 2));
            Assert.AreEqual(expected, result );

            var result2 =  sut.Translate(0,-2);
            var expected2 = new Point(x: 0, y: 2, new SphericSpace( maxX: 1, maxY: 2));
            Assert.AreEqual(expected2, result2 );
        }

           [Test]
        public void  A_Point_Translate_In_SPace_And_Wrap_On_X()
        {
            var sut = new Point(x: 2, y: 1, space: new SphericSpace( maxX: 2, maxY: 1));
            var result = sut.Translate(1,0);
            var expectedLowerBorderX = new Point(x: 0, y: 1, new SphericSpace( maxX: 2, maxY: 1));
            Assert.AreEqual(expectedLowerBorderX, result );

             sut = new Point(x: 0, y: 1, space: new SphericSpace( maxX: 2, maxY: 1));
            var result2 =  sut.Translate(-1,0);
            var expectedUpperBorderX = new Point(x: 2, y: 1, new SphericSpace( maxX: 2, maxY: 1));
            Assert.AreEqual(expectedUpperBorderX, result2 );
        }
    }
}