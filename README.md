## Introduction

- nous avons voulu jouer le Mars Rover kata sur le long cour; et en cherchant un design emergent conduit par les tests (TDD).
- Ce design veut mettre en avant des objets immuables là où cela nous a paru pertinent; et moins d'effet de bord
- Nous poussons le plaisir à mettre en place des Ports & Adapters en archi hexagonale

## Decision Records

- les Adapteurs ne manipulent pas le métier
- les Adapteurs exposent les DTO propres à leur besoins (et capacités)
- le Service  "orchestre" les "flux" entre Adapteurs et Métier

- les tests de l'Adapteur ne doivent pas tester le sous système utilisé (SQL par exemple), par contre ce sous système requiert d'être proprement adressé (par les bonnes requêtes SQL). Les tests vont vérifier que l'on a utilisé le sous-système de la façon approprié.

- Il semble judicieux qu'un langage de plus haut niveau puisse servir à "piloter" (et donc tester) les actions de l'Adapteur de Persistance.

## TODO list

- pour le Repository: 2 options
Option 1:  faire des tests avec une Init/Restore de la réprésentation interne du stockage
Option 2:  faire des tests avec l'enchainement des opération de Write (publiques) avant les opérations de Query 




- Towards Hexagonal architecture with IO services:
    - une map avec ses obstacles dans un fichier
    - une map affichée dans la console


- (still relevant?) circularList -> primitiveObession;  evenCircularList et oddCircularList  (pour connaitre Opposite)
 

### Kata selected
Rover on Mars 
https://kata-log.rocks/mars-rover-kata
with a DDD mindset
https://codereview.stackexchange.com/questions/191991/mars-rover-kata-using-tdd-and-solid

### Langage c-sharp
https://dotnet.microsoft.com/download/dotnet-core/3.1


### using Nunit3 on netCore 3.1


This is a simple pipeline example for a .NET Core application with integrated XUnit tests that are ok for VS Code,
and appear in the .Net Test Explorer tool (first install that extension in your VS Code)
 showing just how easy it is to get up and running with .NET development using GitLab while doing TDD.

# Reference links

- [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)
- [.NET Hello World tutorial](https://dotnet.microsoft.com/learn/dotnet/hello-world-tutorial/)

If you're new to .NET you'll want to check out the tutorial, but if you're
already a seasoned developer considering building your own .NET app with GitLab,
this should all look very familiar.

## What's contained in this project

The root of the repository contains the out of the `dotnet new console` command,
which generates a new console application that just prints out "Hello, World."
It's a simple example, but great for demonstrating how easy GitLab CI is to
use with .NET. Check out the `Program.cs` and `dotnetcore.csproj` files to
see how these work.

In addition to the .NET Core content, there is a ready-to-go `.gitignore` file
sourced from the the .NET Core [.gitignore](https://github.com/dotnet/core/blob/master/.gitignore). This
will help keep your repository clean of build files and other configuration.

Finally, the `.gitlab-ci.yml` contains the configuration needed for GitLab
to build your code. Let's take a look, section by section.

First, we note that we want to use the official Microsoft .NET SDK image
to build our project.

```
image: microsoft/dotnet:latest
```

We're defining two stages here: `build`, and `test`. As your project grows
in complexity you can add more of these.

```
stages:
    - build
    - test
```

Next, we define our build job which simply runs the `dotnet build` command and
identifies the `bin` folder as the output directory. Anything in the `bin` folder
will be automatically handed off to future stages, and is also downloadable through
the web UI.

```
build:
    stage: build
    script:
        - "dotnet build"
    artifacts:
      paths:
        - bin/
```

Similar to the build step, we get our test output simply by running `dotnet test`.

```
test:
    stage: test
    script: 
        - "dotnet test"
```

This should be enough to get you started. There are many, many powerful options 
for your `.gitlab-ci.yml`. You can read about them in our documentation 
[here](https://docs.gitlab.com/ee/ci/yaml/).

test 
