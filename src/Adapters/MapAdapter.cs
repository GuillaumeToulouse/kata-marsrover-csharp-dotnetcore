

using System;
using src.Core.Domain;
using src.Core.Behaviors;
using System.Text.Json;  // on va dire que maintenant que c'est intégré dans System (c'est à dire livré avec le SDK), ce n'est plus une dépendance externe
using Dawn;
using src.Adapters.Dto;

namespace src.Adapters
{

    public class MapAdapter : ICanReadAMap {

        ICanReadFile fileReader;

        public MapAdapter(ICanReadFile fileReader)
        {
            this.fileReader = fileReader;
        }

        public IAmAMap read (string filePath){
            
            var jsonString = fileReader.ReadAllText(filePath);  
            Guard.Argument(jsonString, nameof(jsonString)).NotNull();
            
            var dtoMap = JsonSerializer.Deserialize<Dto.MapForJson>(jsonString);
            //TODO 1: et si ya des erreurs????? lecture fichier KO?  serialisation KO?   
            //SVP si possible sans Exceptions :-p
            
            // la responsabilité de l'adapteur c'est de restituer l'objet métier qui correspond aux données
            return new Map(dtoMap.maxX, dtoMap.maxY);
            
            
        }
    }
}