using src.Core.Domain;
using System.Diagnostics.CodeAnalysis;

namespace src.Adapters.Dto
{
    //Data Transfer Objects are a (fancy) term for an object that carries data between two separate systems.

    //Our DTO is a data contract. We're telling anyone who uses this API, "hey, this is going to be the format that you can always expect to see from this API call".
    //that's why Dto can be  specific to adapters
    //https://khalilstemmler.com/articles/typescript-domain-driven-design/repository-dto-mapper/
    //https://buildplease.com/pages/repositories-dto/
    // instead of writting a DTO by hand, one can use a generic mapper like https://docs.automapper.org/en/stable/Getting-started.html
    public struct ObservableMap : IAmAObservableMap
    {
         
        public int maxX { get; set; }
        
        public int maxY{ get; set; }

     
        public ObservableMap(int abcisseMax, int ordonneeMax)
        {
            maxX = abcisseMax;
            maxY = ordonneeMax;
        }

        public bool Equals([AllowNull] IAmAObservableMap other)
        {
            if (other == null)
                return false;
            return (this.maxX == other.maxX && this.maxY == other.maxY);
        }
    }
}