using src.Core.Domain;
using System.Diagnostics.CodeAnalysis;

namespace src.Adapters.Dto
{
    public struct DtoPositionForRepository 
    {
        int posX;
        int posY;

        CardinalPoints orientation;

        public DtoPositionForRepository(int x, int y, CardinalPoints orientation)
        {
            this.posX = x;
            this.posY = y;
            this.orientation = orientation;
        }

        public int PosX { get => posX; set => posX = value; }
        public int PosY { get => posY; set => posY = value; }
        public CardinalPoints Orientation { get => orientation; set => orientation = value; }


    }
}