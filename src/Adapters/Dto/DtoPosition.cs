using src.Core.Domain;

namespace src
{
    public struct DtoPosition
    {
        readonly DtoPoint point;
        readonly CardinalPoints orientation;

        public DtoPosition(Position positionToSave) : this()
        {
            this.point = new DtoPoint(positionToSave.Point.x, positionToSave.Point.y );
            this.orientation = positionToSave.FrontDirection;
        }

        public DtoPosition(DtoPoint point, CardinalPoints orientation)
        {
            this.point = point;
            this.orientation = orientation;
        }

        public DtoPoint Point => point;

        public CardinalPoints Orientation => orientation;
    }
}