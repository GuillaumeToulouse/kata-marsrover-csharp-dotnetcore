namespace src
{
    public struct DtoPoint 
    {
        readonly int x;
        readonly int y;

        public DtoPoint(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int X => x;

        public int Y => y;
    }
}