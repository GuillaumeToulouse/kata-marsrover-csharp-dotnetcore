using System;
using src.Core.Domain;

namespace src.ServiceLayer
{
    public class RoverDriverService
    {
        private IRepository repo;

        public RoverDriverService()
        {}

        public RoverDriverService(IRepository repo)
        {
            this.repo = repo;
        }
        
        //TO READ:  https://dev.to/barrymcauley/onion-architecture-3fgl
        public CommandResult SendCommand(string command)
        {
            //TODO: faire un adapteur de Command :  string -> business.Command
            if (command=="A")
                return new InvalidCommand();
            
            var metier = new Rover(new Position(new Point(0,0), CardinalPoints.North) );  //TODO: un builder de position par défaut
            var positionToSave = metier.ComputeOneCommand(command);
            var dtoPositionToSave = new DtoPosition(positionToSave);
           if (repo!=null)
                repo.Save(dtoPositionToSave);
            return new ValidCommand();

        }
    }
}