using src.Core.Domain;
using src.Core.Behaviors;
using src.Core.Utilities;
using src.Adapters;

namespace src.ServiceLayer
 {
    public class RoverObservationService : ICanObserveRover
    {
        private IAmAMap businessMap;
        private IRepository repo;

        public RoverObservationService()
        {
            var adapter = new MapAdapter(new IOFileReader() );
            this.businessMap =  adapter.read(filePath : "../../../map.json");            
        }

        public RoverObservationService(IRepository repo): this()
        {          
            this.repo = repo;
        }

        public IAmAMap GiveCurrentMap()
        {            
            return this.businessMap;
        }

        //TODO : remplacer GiveCurrentRoverPosition   par 
                // CurrentMap.Rover.Position        
         /// proche de la version Rest :  GET /CurrentMap/Rover/Position

         // écrire les viewModels des Entités du domaine

        public Position  GiveCurrentRoverPosition()
        {
            var dtoPosition = repo.Query("/Rover/0/position");
            var positionResult = new Position(
                new Point(dtoPosition.PosX, dtoPosition.PosY), dtoPosition.Orientation  ) ;
            return positionResult;
        }
    }

}