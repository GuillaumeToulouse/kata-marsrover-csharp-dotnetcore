namespace src.ServiceLayer
{
    public  interface CommandResult
    {
    }

    public struct ValidCommand : CommandResult
    {
        
    }

    public struct InvalidCommand : CommandResult
    {
        
    }
}