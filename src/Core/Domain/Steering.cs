using System;

namespace src.Core.Domain
{
    public class Steering
    {
        public Orientation orientation {get; private set;}

        public Steering( Orientation orientation)
        {
            this.orientation = orientation;
        }

        public CardinalPoints TurnRight()
        {
                orientation = orientation.TurnRight();
                return orientation.Value;
        }

        public CardinalPoints TurnLeft()
        {
                orientation = orientation.TurnLeft();
                return orientation.Value;
        }
    }
}