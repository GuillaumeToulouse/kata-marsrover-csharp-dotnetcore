

using System;

namespace src.Core.Domain
{
    public class Motor
    {
        private readonly Steering steering;

        public Motor( Steering steering)
        {
            this.steering = steering;
        }

        public Point MoveForward(Point p)
        {
            var orientation = steering.orientation.Value;
            return Move(p, orientation);
        }

        internal Point MoveBackward(Point p)
        {
            var currentOrientation = steering.orientation;
            var oppositeOrientation = currentOrientation.TurnLeft().TurnLeft();
            return Move(p, oppositeOrientation.Value);
        }

        private Point Move(Point p, CardinalPoints orientation)
        {
            if (orientation == CardinalPoints.North)
                return p.Translate(0,1) ;
            if (orientation == CardinalPoints.South)
                return p.Translate(0,-1) ;
            if (orientation == CardinalPoints.West)
                return p.Translate(-1,0) ;
            return p.Translate(1,0) ;
        }


    }
}