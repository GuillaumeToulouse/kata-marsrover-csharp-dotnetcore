namespace src.Core.Domain
{
    public enum CardinalPoints
    {
        North,
        South,
        East,
        West
    }
}