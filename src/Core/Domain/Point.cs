using System;

namespace src.Core.Domain
{
    public struct Point
    {
        public int x;
        public int y;
        private IAmSpace space;

        public Point(int x, int y)
        {
            this.space = new SphericSpace(5, 2);// (int.MaxValue,int.MaxValue);
            this.x = x;
            this.y = y;
        }

        public Point(int x, int y, IAmSpace space) : this(x, y)
        {
            this.space = space;
        }

        public override string ToString()
        {
            return this.x + ":" + this.y;
        }

        public Point Translate(int deltaX, int deltaY)  //todo:  replace les types des arguments deltax et deltaY par un type Pas  ->  PasAvant,  PasArriere
        {
            var newY = space.WrapY(y + deltaY);
            var newX = space.WrapX(x + deltaX);
            return new Point(newX, newY, space);
        }
    }
}