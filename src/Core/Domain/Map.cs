using System;
using src;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics.CodeAnalysis;

namespace src.Core.Domain
{
    public class  Map : IAmAMap
    {

        IAmSpace referentielSpace ;
       
        public Map(int maxX, int maxY)
        {
            referentielSpace = new SphericSpace(maxX: maxX, maxY: maxY);
            obstacles = new List<Point>();
        }

        private Rover currentRover;
        private IList<Point> obstacles;

        public Rover Land( Point point, CardinalPoints faceDirection)
        {
            var pointWithSpace = new Point(point.x, point.y, referentielSpace );
            Position startPos = new Position(pointWithSpace, faceDirection);
            currentRover = new Rover(startPos);
            return currentRover;
        }

        public void TalkTo(Rover identifiantRover1)
        {
            currentRover = identifiantRover1;
        }

        public void SendCommand(string v)
        {
            // suggestion 2: OOP style
            var potentialPosition = currentRover.ComputeOneCommand(v);
            if ( obstacles.Where(obs => obs.Equals(potentialPosition.Point)).Any())
                return;
            currentRover.RunOneCommand(v);
        }

        public Position Localize()
        {
            return currentRover.Position;
        }

        public void Obstacle(int x, int y)
        {
            obstacles.Add(new Point(x,y, referentielSpace)) ;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType()) 
            {
                return false;
            }
            return ( (EqualObstactle((Map)obj) &&
                this.referentielSpace.Equals(((Map)obj).referentielSpace))
                    );
        }

        public bool EqualObstactle(Map obj)
        {
            if (this.obstacles.Count != obj.obstacles.Count)
                return false;
            var res = !(this.obstacles.Except((obj).obstacles).Any());
          
            return res;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return this.referentielSpace.ToString();
        }

        public bool Equals([AllowNull] IAmAMap other)
        {
            throw new NotImplementedException();
        }
    }
}