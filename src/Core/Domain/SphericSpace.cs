using System;

namespace src.Core.Domain
{
    public struct SphericSpace : IAmSpace
    {
        private int maxX;
        private int maxY;

        public SphericSpace(int maxX, int maxY)
        {
            this.maxX = maxX;
            this.maxY = maxY;
        }

        public int MaxX  { get => maxX;}
        public int MaxY  { get => maxY;}

        public int WrapY(int y)
        {
            if (y<0)
                return MaxY;
            if (y > MaxY)
                return 0;
            return y;
        }

        public int WrapX(int x)
        {
            if (x<0)
                return maxX;
            if (x > MaxX)
                return 0;
            return x;
        }

        public override string ToString()
        {
            return string.Format( "SphericSpace maxX={0}, maxY={1}", MaxX, maxY);
        }
    }

    public interface IAmSpace
    {
         int WrapX(int x);
          int WrapY(int y);
    }
}