using System;
using System.Collections.Generic;
using src;

namespace src.Core.Domain
{
    public struct Position
    {

        public Position(Point point, CardinalPoints orientation)
        {
            this.Point = point;
            this.FrontDirection = orientation;
        }

        public readonly Point Point;
        public readonly CardinalPoints FrontDirection;


        public override string ToString()
        {
            return "point=" + this.Point.ToString() + " orientation=" + this.FrontDirection;
        }

        internal Position ChangeDirection(CardinalPoints newDirection)
        {
            return new Position(this.Point, newDirection);
        }

        internal Position MovePoint(Point point)
        {
            return new Position(point, this.FrontDirection);
        }
    }
}