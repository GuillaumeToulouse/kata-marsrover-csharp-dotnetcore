using System;

namespace src.Core.Domain
{
    public class Rover
    {
        public Position Position { get => position; internal set => position = value; }
        readonly Motor motor;
        readonly Steering steering;

        private Position position;

        public Rover(Position startPosition)
        {
            var orientationDeDepart = new Orientation(startPosition.FrontDirection);
            steering = new Steering( orientationDeDepart);
            motor = new Motor(steering);
            Position = startPosition;
        }      

        public Position ComputeOneCommand(string commands)
        {
            if (commands == "F")
               return this.Position.MovePoint(this.motor.MoveForward(position.Point));
            if (commands == "B")
                return  this.Position.MovePoint( this.motor.MoveBackward(position.Point));
            if (commands == "R")
                return this.Position.ChangeDirection(this.steering.TurnRight());
            if (commands == "L")
                return this.Position.ChangeDirection(this.steering.TurnLeft());
            return this.Position;
        }

     
        public void RunOneCommand(string commands)
        {
            if (commands == "F")
                this.Position = this.Position.MovePoint(this.motor.MoveForward(position.Point));
            if (commands == "B")
                this.Position = this.Position.MovePoint( this.motor.MoveBackward(position.Point));
            if (commands == "R")
                this.Position = this.Position.ChangeDirection(this.steering.TurnRight());
            if (commands == "L")
                this.Position = this.Position.ChangeDirection(this.steering.TurnLeft());
        }

    }
}