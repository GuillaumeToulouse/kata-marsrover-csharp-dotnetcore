using System;
using System.Collections;
using src.Core.Utilities;

namespace src.Core.Domain
{
    public struct Orientation
    {
        static ICircularList<CardinalPoints> listeDesOrientations = new CircularList<CardinalPoints>(
            CardinalPoints.North, CardinalPoints.West, CardinalPoints.South, CardinalPoints.East);


        public Orientation(CardinalPoints orientation)
        {
            Value = orientation;
            LeftOrientation = listeDesOrientations.StartAt(orientation).Next(1);
            RightOrientation = listeDesOrientations.Next(2);
        }

        public readonly CardinalPoints Value;
        readonly CardinalPoints LeftOrientation;
        readonly CardinalPoints RightOrientation;

        public Orientation TurnLeft()
        {
            return new Orientation(LeftOrientation);
        }

        public Orientation TurnRight()
        {
            return new Orientation(RightOrientation);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Value);
        }

        public override string ToString()
        {
            return string.Format("orientation {0}", Value);
        }

        public override bool Equals(object obj)
        {
            return this.Value.Equals(((Orientation)obj).Value);
        }

    }
}