using System;
using System.Linq;

namespace src.Core.Utilities
{
    //TODO: should be refactored as a value object / immutable  (no state with currentPosition)
    public class CircularList<T>  : ICircularList<T>
    {
        public override bool Equals(object obj)
        {
            if (this.list.Length != ((CircularList<T>)obj).list.Length)
                return false;
            return !this.list.Except(((CircularList<T>)obj).list).Any();
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(currentPosition, list);
        }
        private int currentPosition;
        readonly T[] list;
        public  int Length { get { return this.list.Length;}}

        public CircularList(params T[] args)
        {
            currentPosition = 0;
            list = args;
        }
        public  CircularList<T>  StartAt(T v)  // TODO: can be removed if we shift the elements of the list , instead of moving the currentPosition
        {
            for (int i = 0; i < list.Length; i++)
            {
                if (list[i].Equals(v))
                {
                    currentPosition = i;
                    break;
                }
            }
            return this;
        }

        public T Next(int step)
        {
            var stepInsideListLength =  step % list.Length;
            currentPosition = currentPosition + stepInsideListLength + list.Length;
            return this.list[currentPosition % list.Length];
        }

        public T Previous(int step)
        {
            return Next( -step);
        }
    }

    public interface ICircularList<T>
    {
        CircularList<T> StartAt(T v);
        T Next(int step);
        T Previous(int step);
    }
}
