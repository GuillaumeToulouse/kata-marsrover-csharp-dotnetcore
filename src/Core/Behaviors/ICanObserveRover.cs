using src.Core.Domain;

namespace  src.Core.Behaviors
{
    public interface ICanObserveRover
    {
        IAmAMap GiveCurrentMap();
        Position GiveCurrentRoverPosition();
    }
}