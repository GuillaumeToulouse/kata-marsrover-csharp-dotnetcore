using System;
using src.Core.Domain;
namespace src.Core.Behaviors
{
    public interface ICanReadAMap {

        //TODO: question:  what represent this string?  an absolute filePath? a URL? a URI? a URN?
        IAmAMap read (string sourceIdentifier);
    }
}