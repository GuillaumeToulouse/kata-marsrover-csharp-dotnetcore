using src.Adapters.Dto;

namespace src
{
    public interface IRepository
    {
        DtoPositionForRepository Query(string v);
        void Save(DtoPosition newPosition);
    }
}