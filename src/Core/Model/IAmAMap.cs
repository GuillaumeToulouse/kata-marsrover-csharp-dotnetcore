using System;

namespace src.Core.Domain
{
    public interface IAmAMap 
    {

    }

    public interface IAmAObservableMap : IAmAMap,  IEquatable<IAmAObservableMap>
    {
        int maxX {get;}
        int maxY {get;}

    }
}